<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O :attribute deve ser aceito.',
    'active_url'           => 'A :attribute não é uma URL válida.',
    'after'                => 'O :attribute deve ser uma data depois :date.',
    'alpha'                => 'O :attribute só pode conter letras.',
    'alpha_dash'           => 'O :attribute só podem conter letras, números e traços.',
    'alpha_num'            => 'O :attribute só pode conter letras e números.',
    'array'                => 'O :attribute deve ser uma matriz.',
    'before'               => 'O :attribute deve ser uma data antes :date.',
    'between'              => [
        'numeric' => 'O :attribute deve situar-se entre :min e :max.',
        'file'    => 'O :attribute deve situar-se entre :min e :max kilobytes.',
        'string'  => 'O :attribute deve situar-se entre :min e :max characters.',
        'array'   => 'O :attribute deve ter entre :min e :max items.',
    ],
    'boolean'              => 'O :attribute campo deve ser verdadeiro ou falso.',
    'confirmed'            => 'O :attribute confirmação não corresponde.',
    'date'                 => 'O :attribute não é uma data válida.',
    'date_format'          => 'O :attribute não coincide com o formato :format.',
    'different'            => 'O :attribute e :other deve ser diferente.',
    'digits'               => 'O :attribute devemos ser:digits dígitos.',
    'digits_between'       => 'O :attribute deve situar-se entre :min e :max dígitos.',
    'distinct'             => 'O :attribute campo tem um valor duplicado.',
    'email'                => 'O :attribute Deve ser um endereço de e-mail válido.',
    'exists'               => 'O selecionado :attribute é inválido.',
    'filled'               => 'O :attribute campo é obrigatório.',
    'image'                => 'O :attribute deve ser uma imagem.',
    'in'                   => 'O selecionado :attribute é inválido.',
    'in_array'             => 'O :attribute campo não existe no :other.',
    'integer'              => 'O :attribute deve ser um número inteiro.',
    'ip'                   => 'O :attribute deve ser um endereço IP válido.',
    'json'                 => 'O :attribute deve ser uma cadeia JSON válido.',
    'max'                  => [
        'numeric' => 'O :attribute não pode ser superior a :max.',
        'file'    => 'O :attribute não pode ser superior a :max kilobytes.',
        'string'  => 'O :attribute não pode ser superior a :max caracteres.',
        'array'   => 'O :attribute não pode ter mais de :max Unid.',
    ],
    'mimes'                => 'O :attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O :attribute deve ser pelo menos :min.',
        'file'    => 'O :attribute deve ser pelo menos :min kilobytes.',
        'string'  => 'O :attribute deve ser pelo menos :min caracteres.',
        'array'   => 'O :attribute deve ter pelo menos :min Unid.',
    ],
    'not_in'               => 'O selecionado :attribute é inválido.',
    'numeric'              => 'O :attribute deve ser um número.',
    'present'              => 'O :attribute campo deve estar presente.',
    'regex'                => 'O :attribute formato é inválido.',
    'required'             => 'O :attribute campo é obrigatório.',
    'required_if'          => 'O :attribute campo é obrigatório quando :other é :value.',
    'required_unless'      => 'O :attribute campo é obrigatório, a menos :other é em :values.',
    'required_with'        => 'O :attribute campo é obrigatório quando :values é presente.',
    'required_with_all'    => 'O :attribute campo é obrigatório quando :values é presente.',
    'required_without'     => 'O :attribute campo é obrigatório quando :values não está presente.',
    'required_without_all' => 'O :attribute campo é obrigatório quando nenhum dos :values estão presente.',
    'same'                 => 'O :attribute e :other devem corresponder.',
    'size'                 => [
        'numeric' => 'O :attribute deve ser :size.',
        'file'    => 'O :attribute deve ser :size kilobytes.',
        'string'  => 'O :attribute deve ser :size caracteres.',
        'array'   => 'O :attribute deve conter :size Unid.',
    ],
    'string'               => 'O :attribute deve ser uma string.',
    'timezone'             => 'O :attribute deve ser uma zona válida.',
    'unique'               => 'O :attribute já existe.',
    'url'                  => 'O :attribute formato é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
