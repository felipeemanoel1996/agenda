<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model{

    protected $hidden = ['created_at', 'updated_at'];

    static function adding($request){

        $create = new Contact();
        $create->user_id = $request->user()->id;
        $create->name = $request->nome;
        $create->phone = $request->telefone;
        $create->email = $request->email;
        $create->date_of_birth = $request->nascimento;
        $create->address = $request->endereco;
        $create->deleted = 'N';
        $create->save();
    }
    static function editing($request, $id){
        $update = Contact::find($id);
        $update->user_id = $request->user()->id;
        $update->name = $request->nome;
        $update->phone = $request->telefone;
        $update->email = $request->email;
        $update->date_of_birth = $request->nascimento;
        $update->address = $request->endereco;
        $update->deleted = $request = 'N';
        $update->save();

    }
    static function destroying($id){

        $delete = Contact::find($id);
        $delete->deleted = 'S';
        $delete->save();
    }
}
