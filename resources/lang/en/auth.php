<?php

return [

    'failed' => 'E-mail ou senha incorreto',
    'throttle' => 'Muitas tentativas de login. Por favor, tente novamente em :seconds segundos.',

];
