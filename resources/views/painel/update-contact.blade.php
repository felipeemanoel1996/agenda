@extends('app')

@section('home')

    <section id="widget-grid" class="">

        <!-- START ROW -->

        <div class="row">

            <!-- NEW COL START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
                    <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Checkout Form</h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">
                            @if(session('success'))
                                <div class="alert alert-block alert-success">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Sucesso</h4>
                                    <p>
                                        {{ session('success') }}
                                    </p>
                                </div>
                            @endif
                            @if(session('DJE'))
                                <div class="alert alert-block alert-warning">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading"> Atenção!</h4>
                                    <p>
                                        {{ session('DJE') }}
                                    </p>
                                </div>
                            @endif
                            @if(session('CNP'))
                                <div class="alert alert-block alert-warning">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading"> Falha ao tentar cadastrar cliente!</h4>
                                    <p>
                                        {{ session('CNP') }}
                                    </p>
                                </div>
                            @endif
                            @if(session('CNE'))
                                <div class="alert alert-block alert-danger">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading"> Atenção!</h4>
                                    <p>
                                        {{ session('CNP') }}
                                    </p>
                                </div>
                            @endif
                            <form id="atulizar-cliente" method="POST" action="{{ url('/update-contact', $contact->id ) }}" class="smart-form" novalidate="novalidate">
                                {{ csrf_field() }}
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-9">
                                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                                <input id="nome" type="text" name="nome" placeholder="Nome*" value="{{ $contact->name }}">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                                <input type="text" name="telefone" placeholder="Telefone*" class="telefone" value="{{ $contact->phone }}">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
                                                <input id="email" type="email" name="email" placeholder="E-mail" value="{{ $contact->email }}">
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="input">
                                                <input type="text" name="nascimento" placeholder="Data de nascimento" class="nascimento" value="{{ $contact->date_of_birth }}">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-9">
                                            <label for="rua" class="input">
                                                <input type="text" name="endereco" id="endereco" placeholder="Endereço" value="{{ $contact->address }}">
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Editar Cliente
                                    </button>
                                </footer>
                            </form>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

        <!-- END ROW -->

    </section>
    <!-- end widget grid -->

@endsection


