@extends('layouts.app')

@section('content')
    <?php

    //initilize the page
    require_once("inc/init.php");

    //require UI configuration (nav, ribbon, etc.)
    require_once("inc/config.ui.php");

    /*---------------- PHP Custom Scripts ---------

    YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
    E.G. $page_title = "Custom Title" */

    $page_title = "Registre-se";

    /* ---------------- END PHP Custom Scripts ------------- */

    //include header
    //you can add your custom css in $page_css array.
    //Note: all css files are inside css/ folder
    $page_css[] = "your_style.css";
    $no_main_header = true;
    $page_html_prop = array("id"=>"extr-page");
    include("inc/header.php");

    ?>
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
    <header id="header">
        <!--<span id="logo"></span>-->

        <div id="logo-group">
            <span id="logo"> <img src="<?php echo ASSETS_URL; ?>/img/logo.png" alt="Pontual TI"> </span>

            <!-- END AJAX-DROPDOWN -->
        </div>

        <span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">Já possui uma conta?</span> <a href="{{ url('/login') }}" class="btn btn-danger">Login</a> </span>

    </header>

    <div id="main" role="main">

        <!-- MAIN CONTENT -->
        <div id="content" class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="well no-padding">

                        <form role="form" method="POST" action="{{ url('/register') }}" id="smart-form-register" class="smart-form client-form">
                            <header>
                                Criar uma conta
                            </header>

                            <fieldset>
                                {{ csrf_field() }}

                                <section>
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nome do Usuário*">
                                            <b class="tooltip tooltip-bottom-right">Por favor, informe seu nome</b> </label>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </section>

                                <section>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail*">
                                            <b class="tooltip tooltip-bottom-right">Por favor, informe seu endereço de e-mail</b> </label>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </section>

                                <section>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Senha">
                                            <b class="tooltip tooltip-bottom-right">Por favor, digite sua senah de acesso</b> </label>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </section>

                                <section>
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirme sua senha">
                                            <b class="tooltip tooltip-bottom-right">Por favor, confirme sua senha de acesso</b> </label>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>
                                    Registrar
                                </button>
                            </footer>

                            <div class="message">
                                <i class="fa fa-check"></i>
                                <p>
                                    Conta criada com sucesso!
                                </p>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- ==========================CONTENT ENDS HERE ========================== -->

    <?php
    //include required scripts
    include("inc/scripts.php");
    ?>

    <!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->

    <script type="text/javascript">
        runAllForms();



        // Validation
        $(function() {
            // Validation
            $("#smart-form-register").validate({

                // Rules for form validation
                rules : {
                    name : {
                        required : true
                    },
                    email : {
                        required : true,
                        email : true
                    },
                    password : {
                        required : true,
                        minlength : 6,
                        maxlength : 20
                    },
                    password_confirmation : {
                        required : true,
                        minlength : 6,
                        maxlength : 20,
                        equalTo : '#password'
                    }
                },

                // Messages for form validation
                messages : {
                    name : {
                        required : 'Por favor, preencher o campo nome de usuário'
                    },
                    email : {
                        required : 'Por favor, preencher o campo de endereço de e-mail',
                        email : 'Por favor, informe um endereço de e-mail válido'
                    },
                    password : {
                        required : 'Por favor, preencher o campo senha',
						minlength : 'Por favor, insira pelo menos 6 caracteres',
						maxlength : 'Por favor, insira no máximo 20 caracteres.'
                    },
                    password_confirmation : {
                        required : 'Por favor, confirme sua senha',
						minlength : 'Por favor, insira pelo menos 6 caracteres',
						maxlength : 'Por favor, insira no máximo 20 caracteres.',
                        equalTo : 'Por favor, digite a mesma senha acima'
                    }

                },


                // Do not change code below
                errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());
                }
            });

        });
    </script>

    <?php
    //include footer
    include("inc/google-analytics.php");
    ?>

@endsection
