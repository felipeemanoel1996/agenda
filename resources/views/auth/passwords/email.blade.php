@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <?php

    //initilize the page
    require_once("inc/init.php");

    //require UI configuration (nav, ribbon, etc.)
    require_once("inc/config.ui.php");

    /*---------------- PHP Custom Scripts ---------

    YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
    E.G. $page_title = "Custom Title" */

    $page_title = "Recuperar Senha";

    /* ---------------- END PHP Custom Scripts ------------- */

    //include header
    //you can add your custom css in $page_css array.
    //Note: all css files are inside css/ folder
    $page_css[] = "your_style.css";
    $no_main_header = true;
    $page_html_prop = array("id"=>"extr-page", "class"=>"animated fadeInDown");
    include("inc/header.php");

    ?>
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
    <header id="header">
        <!--<span id="logo"></span>-->

        <div id="logo-group">
            <span id="logo"> <img src="<?php echo ASSETS_URL; ?>/img/logo.png" alt="Pontual TI"> </span>

            <!-- END AJAX-DROPDOWN -->
        </div>

        <span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">Precisa de uma conta?</span> <a href="{{ url('/register') }}" class="btn btn-danger">Criar uma Conta</a> </span>

    </header>

    <div id="main" role="main">

        <!-- MAIN CONTENT -->
        <div id="content" class="container">

            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="well no-padding">
                        <form role="form" method="POST" action="{{ url('/password/email') }}" id="login-form" class="smart-form client-form">
                            <header>
                                Solicitação de Senha
                            </header>
                            <fieldset>
                                {{ csrf_field() }}
                                <section>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="label">Informe seu endereço de email</label>
                                        <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i>Digite o endereço de e-mail para a redefinir sua senha</b></label>
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif

                                        <div class="note">
                                                <a href="{{ url('/login') }}">Lembrei da minha senha!</a>
                                            </div>
                                    </div>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-refresh"></i> Solicitar Senha
                                </button>
                            </footer>
                        </form>

                    </div>

                </div>
            </div>
        </div>

    </div>

    <!-- END MAIN PANEL -->
    <!-- ==========================CONTENT ENDS HERE ========================== -->


    <?php
    //include required scripts
    include("inc/scripts.php");
    ?>

    <!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->

    <script type="text/javascript">
        runAllForms();

        $(function() {
            // Validation
            $("#login-form").validate({
                // Rules for form validation
                rules : {
                    email : {
                        required : true,
                        email : true
                    }
                },

                // Messages for form validation
                messages : {
                    email : {
                        required : 'Por favor, informe o seu endereço de e-mail',
                        email : 'Por favor, informe um endereço de e-mail válido'
                    }
                },

                // Do not change code below
                errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());
                }
            });
        });
    </script>

    <?php
    //include footer
    include("inc/google-analytics.php");
    ?>




















<!--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
