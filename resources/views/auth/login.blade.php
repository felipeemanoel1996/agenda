@extends('layouts.app')

@section('content')

    <?php

    //initilize the page
    require_once("inc/init.php");

    //require UI configuration (nav, ribbon, etc.)
    require_once("inc/config.ui.php");

    /*---------------- PHP Custom Scripts ---------

    YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
    E.G. $page_title = "Custom Title" */

    $page_title = "Login";

    /* ---------------- END PHP Custom Scripts ------------- */

    //include header
    //you can add your custom css in $page_css array.
    //Note: all css files are inside css/ folder
    $page_css[] = "your_style.css";
    $no_main_header = true;
    $page_html_prop = array("id"=>"extr-page", "class"=>"animated fadeInDown");
    include("inc/header.php");

    ?>
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
    <header id="header">
        <!--<span id="logo"></span>-->

        <div id="logo-group">
            <span id="logo"> <img src="<?php echo ASSETS_URL; ?>/img/logo.png" alt="Pontual TI"> </span>

            <!-- END AJAX-DROPDOWN -->
        </div>

        <span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">Precisa de uma conta?</span> <a href="{{ url('/register') }}" class="btn btn-danger">Criar uma conta</a> </span>

    </header>

    <div id="main" role="main">
        <!-- MAIN CONTENT -->
        <div id="content" class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="well no-padding">
                        <form id="login-form" class="smart-form client-form" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            <header>
                                Entar
                            </header>

                            <fieldset>
                                {{ csrf_field() }}

                                <section>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="label">E-mail</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Favor informe seu e-mail</b></label>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </section>

                                <section>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="label">Senha</label>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input id="password" type="password" class="form-control" name="password">
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Informe sua senha</b> </label>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        <div class="note">
                                            <a class="btn btn-link" href="{{ url('/password/reset') }}">Esqueceu sua senha?</a>
                                        </div>
                                    </div>
                                </section>

                                <!--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>-->

                                <!--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>-->

                                <!--<div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>-->

                                <!--<div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-sign-in"></i> Login
                                        </button>

                                        <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                    </div>
                                </div>-->
                            </fieldset>

                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>
                                    Entrar
                                </button>
                            </footer>
                        </form>

                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- END MAIN PANEL -->
    <!-- ==========================CONTENT ENDS HERE ========================== -->

    <?php
    //include required scripts
    include("inc/scripts.php");
    ?>

    <!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->
    <script type="text/javascript">
        runAllForms();

        $(function() {
            // Validation
            $("#login-form").validate({
                // Rules for form validation
                rules : {
                    email : {
                        required : true,
                        email : true
                    },
                    password : {
                        required : true,
                        minlength : 6,
                    }
                },

                // Messages for form validation
                messages : {
                    email : {
                        required : 'Por favor, preencher o campo de endereço de e-mail',
                        email : 'Por favor, informe um endereço de e-mail válido'
                    },
                    password : {
                        required : 'Por favor, preencher o campo senha',
						minlength : 'Por favor, insira pelo menos 6 caracteres',
                    }
                },

                // Do not change code below
                errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());
                }
            });
        });
    </script>

    <?php
    //include footer
    include("inc/google-analytics.php");
    ?>

@endsection
