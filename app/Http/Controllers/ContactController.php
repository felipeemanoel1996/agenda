<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

use App\Http\Requests;

class ContactController extends Controller{

    public function index(){
        //
    }

    public function create(){

        return view('painel.new-contact');

    }

    public function store(Request $request){

        if($request->nome != '' && $request->telefone != ''){

            $contact = Contact::where('phone', $request->telefone)->first();

            if(!$contact){

                Contact::adding($request);
                return back()->with(['success' => 'Contato cadastrado com sucesso']);

            }else{

                //DJE -> Documento já existe
                return back()->with(['DJE' => 'O número de telefone já existe!']);

            }
        }else{
            //CNP -> Campos não Preenchidos
            return back()->with(['CNP' => 'Você precisa preencha todos os campos obrigatório']);
        }

    }

    public function show($id){
        //
    }

    public function edit($id){

        $contact = Contact::findOrfail($id);

        return view('painel.update-contact', compact('contact'));
    }

    public function update(Request $request, $id){

        $contact = Contact::find($id);

        if($contact){

            $contact = Contact::where('phone', $request->telefone)->where('deleted', '=', 'N')->first();

            if($contact){

                if($contact->id == $id){

                    Contact::editing($request, $id);

                    return back()->with(['success' => 'Dados do Cliente Atualizado com sucesso!']);

                }else{

                    //DJE -> Número já existe
                    return back()->with(['NJE' => 'Já existe um contato cadastrado com o mesmmo número']);

                }

            }else{

                Contact::editing($request, $id);
                return back()->with(['success' => 'Dados do contato atualizado com sucesso!']);
            }

        }else{

            //CNE -> Contato Não Existe
            return back()->with(['CNE' => 'O contato selecionado não existe em nosso banco de dados.']);

        }

    }

    public function destroy($id){

        $contact = Contact::find($id);

        if($contact){
            Contact::destroying($id);
            return back()->with(['deletado' => 'Contato deletado com sucesso']);
        }

    }
}
