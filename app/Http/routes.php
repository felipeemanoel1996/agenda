<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix'=>'/', 'middleware'=>'auth'], function(){
    Route::get('/', ['as'=>'dashboard.index', 'uses'=>'DashboardController@index']);

    Route::get('/clientes', 'ClientRegisterController@index');
    Route::get('/new-contact', 'ContactController@create');
    Route::post('/new-contact', 'ContactController@store');
    Route::get('/update-contact/{id}', 'ContactController@edit');
    Route::post('/update-contact/{id}', 'ContactController@update');
    Route::delete('/deleted-contact/{id}', 'ContactController@update');


});

Route::auth();
Route::get('/home', 'HomeController@index');
