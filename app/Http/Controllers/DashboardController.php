<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;


use App\Http\Requests;

class DashboardController extends Controller{

    public function index(Request $request){

        $contacts = Contact::where('deleted', '=', 'N')->where('user_id', '=', $request->user()->id)->get();

        return view('dashboard', compact('contacts'));

        //return view('dashboard');
    }
}
