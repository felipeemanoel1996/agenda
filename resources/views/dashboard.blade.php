
@extends('app')

@section('home')

    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-subtitle">Exportar para Excel</h6>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Telefone</th>
                                    <th>Email</th>
                                    <th>Data de Nascimento</th>
                                    <th>Endereço</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Nome</th>
                                    <th>Telefone</th>
                                    <th>E-mail</th>
                                    <th>D. Nascimento</th>
                                    <th>Endereço</th>
                                    <th>Ações</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @forelse($contacts as $contact)
                                    <tr>
                                        <td>{{$contact->name}}</td>
                                        <td>{{$contact->phone}}</td>
                                        <td>{{$contact->email}}</td>
                                        <td>{{$contact->date_of_birth}}</td>
                                        <td>{{$contact->address}}</td>
                                        <td>
                                            <form method="POST" action="{{ url('/deleted-contact', $contact->id ) }}">
                                                {{ csrf_field() }}
                                                <a href="{{ url('update-contact', $contact->id) }}" class="btn btn-success">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>
                                                <input type="hidden" name="_method" value="DELETE">

                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletarCliente">
                                                    <i class="fa fa-trash"></i>
                                                </button>

                                                <!--<button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>-->
                                            </form>
                                        </td>
                                    </tr>


                                    <!-- Modal Deletar-->
                                    <div class="modal fade" id="deletarCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Deseja remover: <strong>{{$contact->name}}</strong> do sistema?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                                                    <button type="submit" class="btn btn-primary">Sim</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->




@endsection


